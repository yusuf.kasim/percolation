I didn't add any comments to the program but it's fairly easy to understand.
you have two calsses the first called point that will store the x,y positions
of the lattice and the link to each neighbor in the lik vector. There's 
also an unused integer called link_state you should ignore it i used it before 
and i'm just lazy to remove it.

The way you'll know which link is connected to what is through the lik varaible,
lik[0] gives the link states to the right, [1] up, [2] left and [3] down. 0 if
not linked and 1 if linked.

The other class is p_model that will have a 2d lattice of points that will set
up. the nbr function of p_model will give you the neighbor of a current point.
the numbers corresponds to the same direction of lik.

The graphic at the end is coded pixel by pixle into a (6*d)*(6*d) pixle .ppm 
file. so you'll need a ppm reader to see the final result. to do this I just
made another 6d*6d matrix from the link status and translated it into pixles.
you can see this in setmatrix() and pring_png()(which is wrongly named) 
functions of class p_model.

The main.cpp will just ask you the dimensions of the lattice and the percolation
probability.

I also have a functions file which just contains a random number generator 
which I use in p_model to get a number between 0 and 1 to compare to the 
probability.

I don't have any safe checks if you enter a negative probability or negative
lattice size