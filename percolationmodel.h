#ifndef PERCOLATIONMODEL_H_INCLUDED
#define PERCOLATIONMODEL_H_INCLUDED
#include <iostream>
#include <cstdlib>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <cmath>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <random>
#include <vector>
#include "point.h"

using namespace std;

class p_model
{
private:
    int d;
    vector<vector<point>> lattice;
    vector<vector<int>> lks;
    int Csize;
    double percolationproba;
public:
    p_model(int,double);
    p_model();
    p_model(const p_model &);
    ~p_model();
    vector<int> Nbr(point,int);
    void Set_up(int,double);
    void percolate();
    void setmatrix();
    void print_png();
};


#endif // PERCOLATIONMODEL_H_INCLUDED
