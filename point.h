#ifndef POINT_H_INCLUDED
#define POINT_H_INCLUDED
#include <iostream>
#include <cstdlib>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <cmath>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <random>
#include <vector>

using namespace std;

class point
{
private:
    int link_state;
    int posx;
    int posy;
    vector<int> lik;
public:
    point(int);
    point(); //this is not defined
    point(const point &);
    ~point();
    void Set_XY(int,int);
    void Set_LS(int);
    void Set_LK(int);
    int GetX() const {return posx;}
    int GetY() const {return posy;}
    int GetLS() const {return link_state;}
    vector<int> GetLK() const {return lik;}
};

#endif // POINT_H_INCLUDED
