#include <iostream>
#include <cstdlib>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <cmath>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <random>
#include "functions.h"
#include <vector>

using namespace std;


double randd(double mm, double MM){
    double random= ((double)rand())/(double)RAND_MAX;
    double range = MM - mm;
    return (random*range)+mm;
} // a function used to randomly generate real numbers
