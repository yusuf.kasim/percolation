#include <iostream>
#include <cstdlib>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <cmath>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <random>
#include <vector>
#include "point.h"
#include "percolationmodel.h"
#include "functions.h"

using namespace std;

p_model::p_model(int dd,double per)
: d(dd)
, percolationproba(per)
, lattice(d,vector<point>(d))
, lks(d*6,vector<int>(d*6))
{
    for(int i=0;i<d;i++){
        for (int j=0; j<d;j++){
            lattice[i][j].Set_XY(i,j);
        }
    }
}

/*void p_model::Set_up(int dim,double prob)
{
    d = dim;
    lattice(d,vector<point>(d));
    percolationproba = prob;
    for(int i=0;i<d;i++){
        for (int j=0; j<d;j++){
            lattice[i][j].Set_XY(i,j);
        }
    }
}*/

p_model::p_model()
{
}

p_model::~p_model()
{
}


vector<int> p_model::Nbr(point a,int k)
{
    vector<int> result(2);
    if (k==0){
            result[0]=a.GetX()+1;
            result[1]=a.GetY();
    }
    if (k==1){
            result[0]=a.GetX();
            result[1]=a.GetY()+1;
    }
    if (k==2){
            result[0]=a.GetX()-1;
            result[1]=a.GetY();
    }
    if (k==3){
            result[0]=a.GetX();
            result[1]=a.GetY()-1;
    }
    if(result[0]<0){result[0]=(d-1);}
    if(result[1]<0){result[1]=(d-1);}
    if(result[0]>(d-1)){result[0]=0;}
    if(result[1]>(d-1)){result[1]=0;}
    return result;
}

void p_model::percolate()
{
    for (int i =0;i<d;i++){
        for (int j=0;j<d;j++){
            for (int k =0;k<4;k++){
                vector<int>nb(2);
                vector<int>nbrs(4);
                int l;
                if(k<2){l = k+2;}
                else{l =k-2;}
                nb=Nbr(lattice[i][j],k);
                nbrs = lattice[i][j].GetLK();
                if(nbrs[k]==0){
                    double ran = randd(0,1);
                    if (ran<percolationproba){
                        lattice[i][j].Set_LK(k);
                        lattice[nb[0]][nb[1]].Set_LK(l);
                    }
                }
            }
        }
    }

}

void p_model::setmatrix()
{
    for(int i=0;i<d;i++){
        for(int j=0;j<d;j++){
            vector<int>lksts(4);
            lksts = lattice[i][j].GetLK();

            lks[6*i][6*j]=0;
            lks[6*i][6*j+1]=0;
            lks[6*i+1][6*j]=0;
            lks[6*i+1][6*j+1]=0;

            lks[6*i+4][6*j+4]=0;
            lks[6*i+4][6*j+5]=0;
            lks[6*i+5][6*j+4]=0;
            lks[6*i+5][6*j+5]=0;

            lks[6*i][6*j+4]=0;
            lks[6*i][6*j+5]=0;
            lks[6*i+1][6*j+4]=0;
            lks[6*i+1][6*j+5]=0;

            lks[6*i+4][6*j]=0;
            lks[6*i+4][6*j+1]=0;
            lks[6*i+5][6*j]=0;
            lks[6*i+5][6*j+1]=0;

            if(lksts[0]==1)
            {
                lks[6*i+4][6*j+2]=1;
                lks[6*i+4][6*j+3]=1;
                lks[6*i+5][6*j+2]=1;
                lks[6*i+5][6*j+3]=1;
            }
            if(lksts[0]==0)
            {
                lks[6*i+4][6*j+2]=0;
                lks[6*i+4][6*j+3]=0;
                lks[6*i+5][6*j+2]=0;
                lks[6*i+5][6*j+3]=0;
            }


            if(lksts[1]==1)
            {
                lks[6*i+2][6*j]=1;
                lks[6*i+3][6*j]=1;
                lks[6*i+2][6*j+1]=1;
                lks[6*i+3][6*j+1]=1;
            }
            if(lksts[1]==0)
            {
                lks[6*i+2][6*j]=0;
                lks[6*i+3][6*j]=0;
                lks[6*i+2][6*j+1]=0;
                lks[6*i+3][6*j+1]=0;
            }


            if(lksts[2]==1)
            {
                lks[6*i][6*j+2]=1;
                lks[6*i][6*j+3]=1;
                lks[6*i+1][6*j+2]=1;
                lks[6*i+1][6*j+3]=1;
            }
            if(lksts[2]==0)
            {
                lks[6*i][6*j+2]=0;
                lks[6*i][6*j+3]=0;
                lks[6*i+1][6*j+2]=0;
                lks[6*i+1][6*j+3]=0;
            }


            if(lksts[3]==1)
            {
                lks[6*i+2][6*j+4]=1;
                lks[6*i+3][6*j+4]=1;
                lks[6*i+2][6*j+5]=1;
                lks[6*i+3][6*j+5]=1;
            }
            if(lksts[3]==0)
            {
                lks[6*i+2][6*j+4]=0;
                lks[6*i+3][6*j+4]=0;
                lks[6*i+2][6*j+5]=0;
                lks[6*i+3][6*j+5]=0;
            }


            if (lksts[0]==0&&lksts[1]==0&&lksts[2]==0&&lksts[4]==0)
            {
                lks[6*i+2][6*j+2]=0;
                lks[6*i+2][6*j+3]=0;
                lks[6*i+3][6*j+2]=0;
                lks[6*i+3][6*j+3]=0;
            }
            if (lksts[0]==1||lksts[1]==1||lksts[2]==1||lksts[4]==1)
            {
                lks[6*i+2][6*j+2]=1;
                lks[6*i+2][6*j+3]=1;
                lks[6*i+3][6*j+2]=1;
                lks[6*i+3][6*j+3]=1;
            }
        }
    }
}

void p_model::print_png()
{
    setmatrix();
    ofstream links("output.ppm");
    links << "P3" << endl;
    links << d*6 << " " << d*6 << endl;
    links << "255" << endl;
    for (int i=0;i<d*6;i++){
        for(int j=0;j<d*6;j++){
            if(lks[j][i]==0){links << "255 255 255 ";}
            if(lks[j][i]==1){links << "0 0 0 ";}
        }
        links << endl;
    }
}
