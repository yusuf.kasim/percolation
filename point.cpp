#include <iostream>
#include <cstdlib>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <cmath>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <random>
#include "point.h"

using namespace std;

point::point()
:lik(4)
{
link_state=0;
for (int i =0;i<4;i++){
    lik[i] = 0;
}
}

point::point(int ls)
:   link_state(ls)
{
}

point::point(const point & a)
{
    link_state = a.GetLS();
    posx = a.GetX();
    posy = a.GetY();
    lik = a.GetLK();
}

point::~point()
{
}

void point::Set_LS(int newls)
{
    link_state=newls;
}

void point::Set_XY(int x,int y)
{
    posx=x;
    posy=y;
}

void point::Set_LK(int n)
{
    lik[n] = 1;
}
