#include <iostream>
#include "percolationmodel.h"

using namespace std;

int main()
{
    int sizeofl;
    double proba;
    cout << "enter the size of the lattice you want to percolate" << endl;
    cin >> sizeofl;
    cout << "enter the percolation probability" << endl;
    cin >> proba;
    p_model p(sizeofl,proba);
    p.percolate();
    p.print_png();
}
